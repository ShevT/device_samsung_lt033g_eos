# Release name
PRODUCT_RELEASE_NAME := lt033g

# Inherit some common EOS stuff.
$(call inherit-product, vendor/eos/config/common_full_phone.mk)

# Inherit device configuration
$(call inherit-product, device/samsung/lt033g/device.mk)

## Device identifier. This must come after all inclusions
PRODUCT_NAME := eos_lt033g
PRODUCT_DEVICE := lt033g
PRODUCT_BRAND := Samsung
PRODUCT_MODEL := SM-P601
PRODUCT_MANUFACTURER := Samsung

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRODUCT_NAME=lt033g
